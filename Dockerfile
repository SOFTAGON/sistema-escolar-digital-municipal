FROM directus/directus:9.1.2

LABEL author="Hermes Alves<hermes@softagon.com.br>"
LABEL image="sed"

ENV KEY=255d861b-5ea1-5996-9aa3-922530ec4037
ENV SECRET=6116487b-cda1-52c2-b5b5-c8022c45e237

WORKDIR /directus/
COPY ./uploads /directus/uploads
COPY ./extensions /directus/extensions

EXPOSE 8055
