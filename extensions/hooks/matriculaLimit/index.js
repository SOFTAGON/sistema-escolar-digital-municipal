module.exports = function registerHook ({ filter }, { database, exceptions }) {
    const { InvalidPayloadException } = exceptions;

    async function getTurmaTotal (turmaID) {
        const results = await database("turma")
            .where({ id: turmaID })
            .select("quantidade_alunos");
        return results[0].quantidade_alunos;
    }

    async function getTotalThisTurma (turmaID) {
        const results = await database("matricula")
            .count('id AS CNT')
            .where({ turma: turmaID })
        return results[0].CNT;
    }

    filter('items.create', async (payload, { collection }, { schema }) => {
        if (collection == 'matricula') {
            const turmaTotal = await getTurmaTotal(payload.turma);
            const contagemAtual = await getTotalThisTurma(payload.turma);
            if (contagemAtual >= turmaTotal) {
                throw new InvalidPayloadException('Essa turma tem limite de ' + turmaTotal + ', você não pode mais matricular nesta turma. ' + contagemAtual);
            }
        }

    });
};