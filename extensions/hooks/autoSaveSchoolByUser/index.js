module.exports = function registerHook ({ filter }, { services, exceptions }) {
    const { InvalidPayloadException } = exceptions;
    const { ItemsService } = services;

    function allowedCollection (collection) {
        const allowed = ["escola", "directus_users", "disciplina"];
        const check = allowed.includes(collection);
        if (check)
            return true;
        else
            return false;
    }


    filter('items.create', async (input, { collection }, { accountability, schema }) => {

        if (allowedCollection(collection)) {
            return input;
        } else {
            try {
                const userService = new ItemsService('directus_users', { accountability, schema });
                const user = await userService.readOne(accountability.user, { fields: ['escola'] });

                if (schema.collections[collection].fields.escola) {
                    input.escola = user.escola;
                    return input;
                } else {
                    throw new InvalidPayloadException("Essa coleção não tem ESCOLA: " + collection);
                }

            } catch (err) {
                throw new InvalidPayloadException(err);
            }
        }
    });
};