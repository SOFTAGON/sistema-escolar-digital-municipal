module.exports = function registerHook ({ filter }, { database, exceptions }) {
    const { InvalidPayloadException } = exceptions;

    function searchPayload (payload, search) {
        var arr = [search];
        var resultObj = {};
        var getAllKeys = Object.keys(payload);
        arr.forEach(function (item) {
            getAllKeys.forEach(function (keyName) {
                if (keyName.indexOf(item) !== -1) {
                    resultObj[keyName] = payload[keyName];
                }
            })
        })
        return resultObj;
    }

    async function buscaCamposPreenchidos (params) {
        const result = await database.raw(`select gc.id, professor, ${params.field}, ${params.fim}, p.nome as nome_professor
                from grade_curricular gc 
                LEFT JOIN individuo as p on gc.professor = p.id
                where extract(hour from ${params.field}) = '${params.horaCheia}'
                and gc.id = ${params.id}`)
            .then((res) => {
                return res.rows;
            })
            .catch((e) => {
                console.log("error. see error log for details.");
                console.error(e);
                throw new InvalidPayloadException('Error ao buscar dados existentes no banco de dados');
            });
        return result;
    }

    async function mesmaHora (params) {
        const checaHora = await Promise.all(params.field.map(async (v) => {
            const fim = v.replace("inicio", "fim");
            const valorForm = params.payload[v];
            const [horaCheia, ...minutesD] = valorForm.split(':');
            const p = { field: v, fim: fim, horaCheia: horaCheia, id: params.id };
            const resp = await buscaCamposPreenchidos(p);
            const comparaHora = resp.map((grade) => {
                const [horaBD, ...minutesBD] = grade[v].split(':');
                if (horaCheia == horaBD) {
                    return true
                }
            });
            return comparaHora;

        }));
        if (checaHora[0][0] == true)
            return true;
        else
            return false;

    }


    //Checa os campos que foram preenchidos.
    //Consulta no banco de dados se já existe o mesmo campo preenchido.
    //Consulta no banco de dados se o valor do campo é o mesmo do preenchimento
    //TODO: falta checar se a aula termina antes da outra começar.
    filter('grade_curricular.items.update', async (payload, { keys }) => {
        try {
            const res = searchPayload(payload, "inicio"); // encontra nos campos var com 'inicio'
            const camposPreenchidos = Object.keys(res); //[ 'segunda_hora_inicio', 'terca_hora_inicio' ]
            // console.log('CAMPOS PREENCHIDOS - > ');
            // console.log(camposPreenchidos);
            const p = { field: camposPreenchidos, payload: payload, id: keys };
            const checaHora = await mesmaHora(p);
            console.log('ChecaHora');
            console.log(checaHora);

            if (checaHora) {
                throw new InvalidPayloadException("Professor já está com horário ocupado");
            }

        } catch (err) {
            throw new InvalidPayloadException(err);
        }
    });
};