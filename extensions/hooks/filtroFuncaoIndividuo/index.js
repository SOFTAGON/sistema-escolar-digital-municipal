module.exports = function registerHook ({ action }, { services, exceptions }) {
    const { InvalidPayloadException } = exceptions;
    const { ItemsService } = services;

    action('grade_curricular.items.read', async (payload, { query, accountability, schema }) => {
        try {
            const userService = new ItemsService('equipe_escolar', { accountability, schema });
            const user = await userService.readByQuery({
                filter: { funcao: { _eq: 'professor' } },
            });
            // console.log('userService ---->>>>> ');
            // console.log(user);

            if (true) {
                console.log('FILTRO DE FUNCAO POR INDIVIDUO   ###### ####');
                payload.query.sort = ['disciplina']
                payload.query.filter = { _eq: "2" }
                console.log(payload.query.filter);
                payload.query.filter = null;
                console.log(payload.query);
                return payload;

            }

        } catch (err) {
            throw new InvalidPayloadException(err);
        }
    });
};