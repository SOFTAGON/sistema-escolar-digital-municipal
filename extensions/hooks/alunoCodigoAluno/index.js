module.exports = function registerHook ({ filter }, { exceptions }) {
    const { InvalidPayloadException } = exceptions;

    let guid = () => {
        var today = new Date();
        var prefix = today.getFullYear();
        let s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(0);
        }

        return s4() + '-' + prefix;
    }

    function checkFieldExist (schema, collection) {
        for (const [key, value] of Object.entries(schema.collections)) {
            if (key == collection) {

                if (value.fields.codigo_aluno) //nome do campo é protocolo
                    return true
                else
                    return false
            }
        }
    }
    filter('items.create', async (payload, { collection }, { schema }) => {
        if (collection == 'aluno') {
            if (checkFieldExist(schema, collection)) {
                const registration = guid().toUpperCase();
                if (registration) {
                    payload.codigo_aluno = registration;
                    return {
                        ...payload
                    };
                }
            } else {
                throw new InvalidPayloadException('Não gerou o código do aluno');
            }
        }

    });
};