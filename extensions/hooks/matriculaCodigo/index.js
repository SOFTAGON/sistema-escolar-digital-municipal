module.exports = function registerHook ({ filter }, { exceptions }) {
    const { InvalidPayloadException } = exceptions;

    let generate = (n) => {
        var add = 1, max = 12 - add;   // 12 is the min safe number Math.random() can generate without it starting to pad the end with zeros.   

        if (n > max) {
            return generate(max) + generate(n - max);
        }

        max = Math.pow(10, n + add);
        var min = max / 10; // Math.pow(10, n) basically
        var number = Math.floor(Math.random() * (max - min + 1)) + min;

        return ("" + number).substring(add);
    }

    function checkFieldExist (schema, collection) {
        for (const [key, value] of Object.entries(schema.collections)) {
            if (key == collection) {

                if (value.fields.matricula) //nome do campo é protocolo
                    return true
                else
                    return false
            }
        }
    }
    filter('items.create', async (payload, { collection }, { schema }) => {
        if (collection == 'matricula') {
            if (checkFieldExist(schema, collection)) {
                const registration = generate(10);
                if (registration) {
                    payload.matricula = registration;
                    return {
                        ...payload
                    };
                }
            } else {
                throw new InvalidPayloadException('Não gerou a matricula do aluno');
            }
        }

    });
};