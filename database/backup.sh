#!/bin/bash
read_var() {
     VAR=$(grep -w $1 $2 | xargs)
     IFS="=" read -ra VAR <<< "$VAR"
     echo ${VAR[1]}
}
 
HOSTNAME=$(read_var DB_HOST ../.env)
USERNAME=$(read_var DB_USER ../.env)
PASSWORD=$(read_var DB_PASSWORD ../.env)
DATABASE=$(read_var DB_DATABASE ../.env)

echo "Começando o backup do banco de dados $DATABASE"
export PGPASSWORD="$PASSWORD"

backup_date=`date +%d-%m-%Y-%X`
pg_dump -F p -h $HOSTNAME -U $USERNAME $DATABASE  > $backup_date-p.sql
gzip $backup_date-p.sql
pg_dump -F t -h $HOSTNAME -U $USERNAME $DATABASE | gzip > $backup_date-d.gz
unset PGPASSWORD
echo "Backup completo do $backup_date.gz"