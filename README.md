# Dados de acesso por nível

## Ver todas as escolas

secretariamunicipal@softagon.app
softagon@2021

## Só ver a escola dele mesmo

secretariaescolar@softagon.app
softagon@2021

# Testando o build do Docker:

docker build -t seda:0.1 .  
docker run seda:0.1

## Imagem gerada pelo gitlab

docker pull registry.gitlab.com/softagon/sistema-escolar-digital-municipal:master
